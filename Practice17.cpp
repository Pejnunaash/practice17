﻿#include <iostream>

class Vector {
private:
    double x=2;
    double y=2;
    double z=2;
public:
    void GetVariables() {
        std::cout << "x:" << x << ' ' << "y:" << y << ' ' << "z:" << z << ' '<<'\n';
    }

    void VectorModul() {
        double modul = sqrt((x * x) + (y * y) + (z * z));
        std::cout << "Modul vectora:" << modul;
    }
};

int main()
{
    Vector v1;
    v1.GetVariables();
    v1.VectorModul();
}

